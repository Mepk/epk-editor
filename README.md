# EPK Editor

#### 介绍
铃心自定义专用代码编辑器（EPK Editor）

#### 说明
[更新说明](https://gitee.com/Mepk/epk-editor/blob/master/program/%E6%9B%B4%E6%96%B0%E8%AF%B4%E6%98%8E.txt)  
[使用说明](https://gitee.com/Mepk/epk-editor/blob/master/program/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.txt)

#### 动图介绍
![界面介绍](https://images.gitee.com/uploads/images/2020/0908/113756_ba610b85_8020813.gif "动图说明.gif")